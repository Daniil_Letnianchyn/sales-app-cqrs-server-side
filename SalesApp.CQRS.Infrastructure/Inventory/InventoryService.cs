﻿using SalesApp.CQRS.Commands.Interfaces;

namespace SalesApp.CQRS.Infrastructure.Inventory
{
    public class InventoryService : IInventoryService
    {
        public void NotifySaleOcurred()
        {
            NotifyNoOne();
        }

        private void NotifyNoOne()
        {
        }
    }
}
