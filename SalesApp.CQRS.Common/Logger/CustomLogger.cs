﻿namespace SalesApp.CQRS.Common.Logger
{
    public class CustomLogger : ICustomLogger
    {
        public void Debug(params string[] list)
        {
            this.DoNothing();
        }

        public void Error(params string[] list)
        {
            this.DoNothing();
        }

        public void Info(params string[] list)
        {
            this.DoNothing();
        }

        public void Warn(params string[] list)
        {
            this.DoNothing();
        }

        private void DoNothing()
        {

        }
    }
}
