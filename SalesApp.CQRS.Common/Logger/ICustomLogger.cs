﻿namespace SalesApp.CQRS.Common.Logger
{
    public interface ICustomLogger
    {
        void Info(params string[] list);
        void Warn(params string[] list);
        void Debug(params string[] list);
        void Error(params string[] list);
    }
}
