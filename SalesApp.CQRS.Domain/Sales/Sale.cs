﻿using SalesApp.CQRS.Domain.Customers;
using SalesApp.CQRS.Domain.Employees;
using SalesApp.CQRS.Domain.Products;
using System;

namespace SalesApp.CQRS.Domain.Sales
{
    public class Sale
    {
        private int _quantity;
        private decimal _totalPrice;
        private decimal _unitPrice;
        private bool _isDiscountAlreadyAplied;
        private Guid _saleId;


        public Sale()
        {
            _isDiscountAlreadyAplied = false;
        }

        public string SaleId
        {
            get { return _saleId.ToString(); }
            set
            {
                _saleId = Guid.Parse(value);
            }
        }

        public DateTimeOffset Date { get; set; }

        public Customer Customer { get; set; }

        public Employee Employee { get; set; }

        public Product Product { get; set; }

        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set
            {
                _unitPrice = value;
            }
        }

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                UpdateUnitPrice();
            }
        }

        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set
            {
                _totalPrice = value;
                UpdateUnitPrice();
            }
        }

        public void ApplyRandomDiscount()
        {
            if (!_isDiscountAlreadyAplied)
            {
                UnitPrice = UnitPrice / 100 * new Random().Next(50, 99);
                _isDiscountAlreadyAplied = true;
            }
        }

        private void UpdateTotalPrice()
        {
            _totalPrice = _unitPrice * _quantity;
        }

        private void UpdateUnitPrice()
        {
            if (_totalPrice != 0 && _quantity != 0)
                _unitPrice = _totalPrice / _quantity;
        }
    }
}
