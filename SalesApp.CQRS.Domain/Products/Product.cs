﻿using System;

namespace SalesApp.CQRS.Domain.Products
{
    public class Product
    {
        private Guid _productId;
        public string ProductId
        {
            get { return _productId.ToString(); }
            set
            {
                _productId = Guid.Parse(value);
            }
        }
        public decimal Price { get; set; }
        public string Name { get; set; }
    }
}
