﻿using SalesApp.CQRS.Commands.Interfaces;
using SalesApp.CQRS.Commands.Sales.CreateSale.Exceptions;
using SalesApp.CQRS.Common.Logger;
using System;

namespace SalesApp.CQRS.Commands.Sales.CreateSale
{
    public class CreateSaleCommand : ICreateSaleCommand
    {
        private IDatabaseService _databaseService;
        private ICustomLogger _logger;
        private IInventoryService _inventory;

        public CreateSaleCommand(IDatabaseService databaseService, ICustomLogger logger, IInventoryService inventory)
        {
            this._databaseService = databaseService;
            this._logger = logger;
            this._inventory = inventory;
        }

        public void Execute(CreateSaleModel saleForCreation)
        {
            try
            {
                Guid.Parse(saleForCreation.CustomerId);
            }
            catch (FormatException)
            {
                throw new InvalidGuidException($@"{saleForCreation.CustomerId}", $@"Imaginary-correlation-id");
            }

            Domain.Sales.Sale newSale = new Domain.Sales.Sale
            {
                Customer = new Domain.Customers.Customer { CustomerId = saleForCreation.CustomerId },
                Employee = new Domain.Employees.Employee { EmployeeId = saleForCreation.EmployeeId },
                Product = new Domain.Products.Product { ProductId = saleForCreation.ProductId },
                Quantity = saleForCreation.Quantity
            };

            _databaseService.CreateSale(newSale);
            _logger.Info($@"Sale {newSale.SaleId} was crated");
            _inventory.NotifySaleOcurred();
        }

    }
}
