﻿namespace SalesApp.CQRS.Commands.Sales.CreateSale
{
    public interface ICreateSaleCommand
    {
        public void Execute(CreateSaleModel model);
    }
}
