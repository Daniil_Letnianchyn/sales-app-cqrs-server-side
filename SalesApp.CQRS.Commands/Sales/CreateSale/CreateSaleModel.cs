﻿namespace SalesApp.CQRS.Commands.Sales.CreateSale
{
    public class CreateSaleModel
    {
        public string CustomerId { get; set; }
        public string EmployeeId { get; set; }
        public string ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
