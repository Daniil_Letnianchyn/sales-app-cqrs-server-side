﻿using System;

namespace SalesApp.CQRS.Commands.Sales.CreateSale.Exceptions
{
    [Serializable]
    public class InvalidGuidException : ApplicationException
    {
        public string CorrelationId { get; }

        public InvalidGuidException(string correlationId)
        {
            this.CorrelationId = correlationId;
        }

        public InvalidGuidException(string invalidGuid, string correlationId)
            : base($@"Invalid Guid: {invalidGuid}")
        {
            this.CorrelationId = correlationId;
        }

    }
}
