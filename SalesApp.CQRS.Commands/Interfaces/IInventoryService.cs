﻿namespace SalesApp.CQRS.Commands.Interfaces
{
    public interface IInventoryService
    {
        public void NotifySaleOcurred();
    }
}
