﻿namespace SalesApp.CQRS.Commands.Interfaces
{
    public interface IDatabaseService
    {
        public void CreateSale(SalesApp.CQRS.Domain.Sales.Sale saleForCreation);
    }
}
