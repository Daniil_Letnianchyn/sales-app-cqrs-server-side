﻿using System.Collections.Generic;

namespace SalesApp.CQRS.Queries.Customers.GetCustomersList
{
    public interface IGetCustomersListQuery
    {
        public IEnumerable<CustomerModel> Execute();
    }
}
