﻿using SalesApp.CQRS.RelationalDbContext;
using System.Collections.Generic;
using System.Linq;

namespace SalesApp.CQRS.Queries.Customers.GetCustomersList
{
    public class GetCustomersListQuery : IGetCustomersListQuery
    {
        private DatabaseContext _databaseContext;

        public GetCustomersListQuery(DatabaseContext databaseContext)
        {
            this._databaseContext = databaseContext;
        }
        public IEnumerable<CustomerModel> Execute()
        {
            return _databaseContext.Customers
               .Select(p => new CustomerModel
               {
                   Id = p.CustomerId.ToString(),
                   Name = p.Name
               })
               .ToList();
        }
    }
}
