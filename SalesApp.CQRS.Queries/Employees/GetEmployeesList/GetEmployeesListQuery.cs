﻿using SalesApp.CQRS.RelationalDbContext;
using System.Collections.Generic;
using System.Linq;

namespace SalesApp.CQRS.Queries.Employees.GetEmployeesList
{
    public class GetEmployeesListQuery : IGetEmployeesListQuery
    {
        private DatabaseContext _databaseContext;

        public GetEmployeesListQuery(DatabaseContext databaseContext)
        {
            this._databaseContext = databaseContext;
        }
        public IEnumerable<EmployeeModel> Execute()
        {
            return _databaseContext.Employees
                .Select(p => new EmployeeModel
                {
                    Id = p.EmployeeId.ToString(),
                    Name = p.Name
                })
                .ToList();
        }
    }
}
