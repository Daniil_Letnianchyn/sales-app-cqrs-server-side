﻿using System.Collections.Generic;

namespace SalesApp.CQRS.Queries.Employees.GetEmployeesList
{
    public interface IGetEmployeesListQuery
    {
        public IEnumerable<EmployeeModel> Execute();
    }
}
