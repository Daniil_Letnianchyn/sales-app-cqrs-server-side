﻿namespace SalesApp.CQRS.Queries.Employees.GetEmployeesList
{
    public class EmployeeModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
