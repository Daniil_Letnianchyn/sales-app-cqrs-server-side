﻿using System.Collections.Generic;

namespace SalesApp.CQRS.Queries.Products.GetProductsList
{
    public interface IGetProductsListQuery
    {
        public IEnumerable<ProductModel> Execute();
    }
}
