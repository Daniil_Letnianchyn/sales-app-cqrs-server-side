﻿namespace SalesApp.CQRS.Queries.Products.GetProductsList
{
    public class ProductModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
