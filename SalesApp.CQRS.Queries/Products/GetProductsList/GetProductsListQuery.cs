﻿using SalesApp.CQRS.RelationalDbContext;
using System.Collections.Generic;
using System.Linq;

namespace SalesApp.CQRS.Queries.Products.GetProductsList
{
    public class GetProductsListQuery : IGetProductsListQuery
    {
        private DatabaseContext _databaseContext;

        public GetProductsListQuery(DatabaseContext databaseContext)
        {
            this._databaseContext = databaseContext;
        }
        public IEnumerable<ProductModel> Execute()
        {
            return _databaseContext.Products
               .Select(p => new ProductModel
               {
                   Id = p.ProductId.ToString(),
                   Name = p.Name,
                   Price = p.Price
               })
               .ToList();
        }
    }
}
