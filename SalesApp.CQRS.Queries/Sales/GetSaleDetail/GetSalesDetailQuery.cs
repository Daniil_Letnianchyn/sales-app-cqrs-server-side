﻿using SalesApp.CQRS.RelationalDbContext;
using System;
using System.Linq;

namespace SalesApp.CQRS.Queries.Sales.GetSaleDetail
{
    public class GetSalesDetailQuery : IGetSalesDetailQuery
    {
        private DatabaseContext _databaseContext;

        public GetSalesDetailQuery(DatabaseContext databaseContext)
        {
            this._databaseContext = databaseContext;
        }
        public SaleDetailModel Execute(string saleId)
        {
            return _databaseContext.Sales
                .Where(i => i.SaleId == Guid.Parse(saleId))
                .Select(p => new SaleDetailModel
                {
                    Id = p.SaleId.ToString(),
                    Date = p.DateCreated,
                    Customer = p.Customer.Name,
                    Employee = p.Employee.Name,
                    Product = p.Product.Name,
                    Quantity = p.Quantity,
                    TotalPrice = p.TotalPrice,
                    UnitPrice = p.TotalPrice / p.Quantity
                })
                .SingleOrDefault();
        }
    }
}
