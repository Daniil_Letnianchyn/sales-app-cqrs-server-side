﻿namespace SalesApp.CQRS.Queries.Sales.GetSaleDetail
{
    public interface IGetSalesDetailQuery
    {
        public SaleDetailModel Execute(string saleId);
    }
}
