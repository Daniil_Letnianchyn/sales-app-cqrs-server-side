﻿using SalesApp.CQRS.RelationalDbContext;
using System.Collections.Generic;
using System.Linq;

namespace SalesApp.CQRS.Queries.Sales.GetSalesList
{
    public class GetSalesListQuery : IGetSalesListQuery
    {
        private DatabaseContext _databaseContext;

        public GetSalesListQuery(DatabaseContext databaseContext)
        {
            this._databaseContext = databaseContext;
        }
        public IEnumerable<SaleModel> Execute()
        {
            return _databaseContext.Sales
                .Select(p => new SaleModel
                {
                    Id = p.SaleId.ToString(),
                    Date = p.DateCreated,
                    Customer = p.Customer.Name,
                    Employee = p.Employee.Name,
                    Product = p.Product.Name,
                    Quantity = p.Quantity,
                    TotalPrice = p.TotalPrice,
                    UnitPrice = p.TotalPrice / p.Quantity
                })
                .ToList();
        }
    }
}
