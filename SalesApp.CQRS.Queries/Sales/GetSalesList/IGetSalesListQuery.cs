﻿using System.Collections.Generic;

namespace SalesApp.CQRS.Queries.Sales.GetSalesList
{
    public interface IGetSalesListQuery
    {
        public IEnumerable<SaleModel> Execute();
    }
}
