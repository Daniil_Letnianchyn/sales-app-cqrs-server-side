﻿using Microsoft.AspNetCore.Mvc;
using SalesApp.CQRS.Commands.Sales.CreateSale.Exceptions;
using SalesApp.CQRS.Queries.Employees.GetEmployeesList;
using System;
using System.Threading.Tasks;

namespace SalesApp.CQRS.WebAPI.Controllers
{
    [Route("api/employees")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private IGetEmployeesListQuery _getEmployeesListQuery;

        public EmployeesController(IGetEmployeesListQuery getEmployeesListQuery)
        {
            this._getEmployeesListQuery = getEmployeesListQuery;
        }

        [HttpGet()]
        public async Task<IActionResult> GetEmployeesAsync()
        {
            try
            {
                return Ok(_getEmployeesListQuery.Execute());
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}