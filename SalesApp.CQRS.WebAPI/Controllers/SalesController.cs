﻿using Microsoft.AspNetCore.Mvc;
using SalesApp.CQRS.Commands.Sales.CreateSale;
using SalesApp.CQRS.Commands.Sales.CreateSale.Exceptions;
using SalesApp.CQRS.Queries.Sales.GetSaleDetail;
using SalesApp.CQRS.Queries.Sales.GetSalesList;
using System;
using System.Threading.Tasks;

namespace SalesApp.CQRS.WebAPI.Controllers
{
    [Route("api/sales")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private IGetSalesDetailQuery _getSalesDetailQuery;
        private IGetSalesListQuery _getSalesListQuery;
        private ICreateSaleCommand _createSaleCommand;

        public SalesController(IGetSalesDetailQuery getSalesDetailQuery,
            IGetSalesListQuery getSalesListQuery,
            ICreateSaleCommand createSaleCommand)
        {
            this._getSalesDetailQuery = getSalesDetailQuery;
            this._getSalesListQuery = getSalesListQuery;
            this._createSaleCommand = createSaleCommand;
        }

        [HttpGet()]
        public async Task<IActionResult> GetSalesAsync()
        {
            try
            {
                return Ok(_getSalesListQuery.Execute());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("{saleId}")]
        public async Task<IActionResult> GetSaleByIdAsync(string saleId)
        {
            try
            {
                return Ok(_getSalesDetailQuery.Execute(saleId));
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost()]
        public async Task<IActionResult> AddSaleAsync([FromBody] SalesApp.CQRS.Commands.Sales.CreateSale.CreateSaleModel saleForCreationDTO)
        {
            try
            {
                _createSaleCommand.Execute(saleForCreationDTO);
                return Ok(new SaleModel());
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}