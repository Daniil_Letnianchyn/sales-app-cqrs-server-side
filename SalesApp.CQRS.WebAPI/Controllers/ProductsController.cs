﻿using Microsoft.AspNetCore.Mvc;
using SalesApp.CQRS.Commands.Sales.CreateSale.Exceptions;
using SalesApp.CQRS.Queries.Products.GetProductsList;
using System;
using System.Threading.Tasks;

namespace SalesApp.CQRS.WebAPI.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IGetProductsListQuery _getProductsListQuery;

        public ProductsController(IGetProductsListQuery getProductsListQuery)
        {
            this._getProductsListQuery = getProductsListQuery;
        }

        [HttpGet()]
        public async Task<IActionResult> GetProductsAsync()
        {
            try
            {
                return Ok(_getProductsListQuery.Execute());
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}