﻿using Microsoft.AspNetCore.Mvc;
using SalesApp.CQRS.Commands.Sales.CreateSale.Exceptions;
using SalesApp.CQRS.Queries.Customers.GetCustomersList;
using System;
using System.Threading.Tasks;

namespace SalesApp.CQRS.WebAPI.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private IGetCustomersListQuery _getCustomersListQuery;

        public CustomersController(IGetCustomersListQuery getCustomersListQuery)
        {
            this._getCustomersListQuery = getCustomersListQuery;
        }

        [HttpGet()]
        public async Task<IActionResult> GetCustomersAsync()
        {
            try
            {
                return Ok(_getCustomersListQuery.Execute());
            }
            catch (InvalidGuidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}