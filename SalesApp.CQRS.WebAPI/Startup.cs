using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SalesApp.CQRS.Commands.Interfaces;
using SalesApp.CQRS.Commands.Sales.CreateSale;
using SalesApp.CQRS.Common.Logger;
using SalesApp.CQRS.Infrastructure.Inventory;
using SalesApp.CQRS.Persistence;
using SalesApp.CQRS.Queries.Customers.GetCustomersList;
using SalesApp.CQRS.Queries.Employees.GetEmployeesList;
using SalesApp.CQRS.Queries.Products.GetProductsList;
using SalesApp.CQRS.Queries.Sales.GetSaleDetail;
using SalesApp.CQRS.Queries.Sales.GetSalesList;
using SalesApp.CQRS.RelationalDbContext;

namespace SalesApp.CQRS.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<DatabaseContext>(options =>
            {
                options.UseSqlServer(
                    @"Server=(localdb)\mssqllocaldb;Database=SalesAppDB;Trusted_Connection=True;");
            });

            services.AddScoped<IDatabaseService, DatabaseService>();
            services.AddScoped<IInventoryService, InventoryService>();
            services.AddScoped<ICreateSaleCommand, CreateSaleCommand>();
            services.AddScoped<IGetSalesListQuery, GetSalesListQuery>();
            services.AddScoped<IGetSalesDetailQuery, GetSalesDetailQuery>();
            services.AddScoped<IGetProductsListQuery, GetProductsListQuery>();
            services.AddScoped<IGetEmployeesListQuery, GetEmployeesListQuery>();
            services.AddScoped<IGetCustomersListQuery, GetCustomersListQuery>();
            services.AddSingleton<ICustomLogger, CustomLogger>();

            // Configure CORS so the API allows requests from JavaScript.  
            // For demo purposes, all origins/headers/methods are allowed.  
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOriginsHeadersAndMethods",
                    builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors("AllowAllOriginsHeadersAndMethods");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
