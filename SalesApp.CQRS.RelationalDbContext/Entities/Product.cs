﻿using SalesApp.CQRS.RelationalDbContext.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace SalesApp.CQRS.RelationalDbContext.Entities
{
    public class Product : IModificationHistory
    {
        [Key]
        public Guid ProductId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }

        public DateTimeOffset DateModified { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
}
