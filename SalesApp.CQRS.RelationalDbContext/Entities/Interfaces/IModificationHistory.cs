﻿using System;

namespace SalesApp.CQRS.RelationalDbContext.Entities.Interfaces
{
    public interface IModificationHistory
    {
        DateTimeOffset DateModified { get; set; }
        DateTimeOffset DateCreated { get; set; }
    }
}
