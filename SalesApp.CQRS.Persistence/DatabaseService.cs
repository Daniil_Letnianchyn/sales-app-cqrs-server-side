﻿using SalesApp.CQRS.Commands.Interfaces;
using SalesApp.CQRS.RelationalDbContext;
using System;
using System.Linq;

namespace SalesApp.CQRS.Persistence
{
    public class DatabaseService : IDatabaseService
    {
        private DatabaseContext _dbContext;
        public DatabaseService(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void CreateSale(SalesApp.CQRS.Domain.Sales.Sale saleForCreation)
        {
            RelationalDbContext.Entities.Sale saleEntity = new RelationalDbContext.Entities.Sale
            {
                Customer = new RelationalDbContext.Entities.Customer { CustomerId = Guid.Parse(saleForCreation.Customer.CustomerId) },
                Employee = new RelationalDbContext.Entities.Employee { EmployeeId = Guid.Parse(saleForCreation.Employee.EmployeeId) },
                Product = new RelationalDbContext.Entities.Product { ProductId = Guid.Parse(saleForCreation.Product.ProductId) },
                Quantity = saleForCreation.Quantity,
                TotalPrice = saleForCreation.Quantity * _dbContext.Products.Where(i => i.ProductId == Guid.Parse(saleForCreation.Product.ProductId)).Select(v => v.Price).SingleOrDefault()
            };
            _dbContext.Attach(saleEntity);
            _dbContext.SaveChanges();
        }
    }
}
